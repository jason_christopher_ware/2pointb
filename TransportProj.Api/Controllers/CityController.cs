﻿using System.Web.Http;
using TransportProj.Business;
using TransportProj.Dto.Models;

namespace TransportProj.Api.Controllers
{
    public class CityController : ApiController
    {
        private readonly CityService _cityService;

        public CityController()
        {
            _cityService = new CityService();
        }

        [HttpGet]
        public IHttpActionResult GenerateCity(int cityWidth, int cityHeight)
        {
            var result = _cityService.GenerateCity(cityWidth, cityHeight);

            if (result.IsSuccessful)
            {
                return Ok(result);
            }

            return NotFound();
        }

        public IHttpActionResult Tick(CityViewModel city)
        {
            var result = _cityService.Tick(city);

            if (result.IsSuccessful)
            {
                return Ok(result);
            }

            return NotFound();
        }
    }
}