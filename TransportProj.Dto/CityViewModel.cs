﻿using System.Runtime.Serialization;
using TransportProj.Common;

namespace TransportProj.Dto
{
    [DataContract]
    public class CityViewModel
    {
        [DataMember]
        public City City { get; set; }

        [DataMember]
        public Car Car { get; set; }

        [DataMember]
        public Passenger Passenger { get; set; }
    }
}
