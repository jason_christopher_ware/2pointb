﻿using System.Runtime.Serialization;
using TransportProj.Dto.Dtos;

namespace TransportProj.Dto.Models
{
    [DataContract]
    public class CityViewModel
    {
        [DataMember]
        public CityDto CityDto { get; set; }
    }
}
