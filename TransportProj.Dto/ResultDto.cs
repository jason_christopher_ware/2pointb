﻿using System.Runtime.Serialization;

namespace TransportProj.Dto
{
    [DataContract]
    public class ResultDto<T>
    {
        [DataMember]
        public T Data { get; set; }
        [DataMember]
        public bool IsSuccessful { get; set; }
    }

}
