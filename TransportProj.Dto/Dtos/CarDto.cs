﻿using System.Runtime.Serialization;
using TransportProj.Common.Enums;

namespace TransportProj.Dto.Dtos
{
    [DataContract]
    public class CarDto
    {
        [DataMember]
        public int XPos { get; set; }
        [DataMember]
        public int YPos { get; set; }
        [DataMember]
        public CarType CarType { get; set; }
    }
}
