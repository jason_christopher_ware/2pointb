﻿using System.Runtime.Serialization;
using TransportProj.Common.Enums;

namespace TransportProj.Dto.Dtos
{
    [DataContract]
    public class CityPositionDto
    {
        [DataMember]
        public CarDto CarDto { get; set; }
        [DataMember]
        public PassengerDto PassengerDto { get; set; }
        [DataMember]
        public PositionType PositionType { get; set; }
        [DataMember]
        public bool IsDestinationPosition { get; set; }
    }
}
