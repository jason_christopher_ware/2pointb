﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TransportProj.Dto.Dtos
{
    [DataContract]
    public class CityDto
    {
        public CityDto()
        {
            CityPositionDtos = new List<CityPositionDto>();
        }

        [DataMember]
        public int YMax { get; set; }
        [DataMember]
        public int XMax { get; set; }
        [DataMember]
        public IList<CityPositionDto> CityPositionDtos { get; set; }
    }
}
