﻿using System.Runtime.Serialization;

namespace TransportProj.Dto.Dtos
{
    [DataContract]
    public class PassengerDto
    {
        [DataMember]
        public int StartingXPos { get; set; }
        [DataMember]
        public int StartingYPos { get; set; }
        [DataMember]
        public int DestinationXPos { get; set; }
        [DataMember]
        public int DestinationYPos { get; set; }
       
    }
}
