﻿using TransportProj.Common.Enums;

namespace TransportProj.Common.Interfaces
{
    public interface ICarFactory
    {
        Car Create(CarType type, int xPos, int yPos, City city, Passenger passenger);
    }
}
