﻿namespace TransportProj.Common.Interfaces
{
    public interface ICityFactory
    {
        City Create(ICarFactory carFactory, int cityWidth, int cityHeight);
    }
}
