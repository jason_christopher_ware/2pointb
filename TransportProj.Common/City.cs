﻿
using System.Linq;
using TransportProj.Common.Enums;
using TransportProj.Common.Interfaces;

namespace TransportProj.Common
{
    public class City
    {
        private readonly ICarFactory _carFactory;
        public int YMax { get; private set; }
        public int XMax { get; private set; }
        public CityPosition[] CityPositions { get; private set; }
        public Car Car { get; private set; }
        public Passenger Passenger { get; private set; }
        public CarType CarType { get; private set; }

        public City(ICarFactory carFactory, int xMax, int yMax, int[] buildingPositions)
        {
            _carFactory = carFactory;
            XMax = xMax;
            YMax = yMax;

            CityPositions = new CityPosition[xMax * yMax];
            for (int xCol = 0; xCol < xMax; xCol++)
            {
                for (int yRow = 0; yRow < yMax; yRow++)
                {
                    var index = yRow * yMax + xCol;
                    var type = buildingPositions.Contains(index)
                        ? PositionType.Building
                        : PositionType.Position;
                    CityPositions[yRow * yMax + xCol] = new CityPosition(type);
                }
            }
        }

        public Car AddCarToCity(CarType carType, int xPos, int yPos)
        {
            Car car = _carFactory.Create(carType, xPos, yPos, this, null);

            CityPositions[yPos * YMax + xPos].Car = car;

            Car = car;
            CarType = carType;

            return car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            CityPositions[startYPos * YMax + startXPos].Passenger = passenger;
            CityPositions[destYPos * YMax + destXPos].IsDestinationPosition = true;

            Passenger = passenger;

            return passenger;
        }

    }
}
