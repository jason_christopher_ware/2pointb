﻿
using TransportProj.Common.Enums;

namespace TransportProj.Common
{
    public class CityPosition
    {
        public CityPosition(PositionType type)
        {
            PositionType = type;
        }
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }
        public PositionType PositionType { get; set; }
        public bool IsDestinationPosition { get; set; }
    }
}
