﻿namespace TransportProj.Common.Enums
{
    public enum DirectionToMove
    {
        DoNothing,
        Left,
        Right,
        Up,
        Down,
        Arrived
    }
}
