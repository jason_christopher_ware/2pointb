﻿namespace TransportProj.Common.Enums
{
    public enum CarType
    {
        Sedan,
        RaceCar,
        Motorcycle
    }
}
