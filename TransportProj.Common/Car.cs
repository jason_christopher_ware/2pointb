﻿using System;

namespace TransportProj.Common
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public int GetTargetXPos(Passenger prospectivePassenger)
        {
            return HasPassenger
                ? Passenger.DestinationXPos
                : prospectivePassenger.StartingXPos;
        }

        public int GetTargetYPos(Passenger prospectivePassenger)
        {
            return HasPassenger
                ? Passenger.DestinationYPos
                : prospectivePassenger.StartingYPos;
        }

        public bool HasPassenger
        {
            get { return Passenger != null; }
        }

        public abstract void MoveUp(Passenger prospectivePassenger);

        public abstract void MoveDown(Passenger prospectivePassenger);

        public abstract void MoveRight(Passenger prospectivePassenger);

        public abstract void MoveLeft(Passenger prospectivePassenger);

        public bool MoveInXDirection(Passenger prospectivePassenger, int numOfSpaces, bool isRight)
        {
            if (numOfSpaces > 0)
            {
                var oldXPos = XPos;

                if (isRight)
                {

                    var possibleIndex = YPos * City.YMax + (XPos + numOfSpaces);
                    if (XPos + numOfSpaces <= City.XMax
                        && XPos + numOfSpaces <= GetTargetXPos(prospectivePassenger))
                    {
                        XPos += numOfSpaces;

                        UpdateCityPosition(oldXPos, YPos);
                        return true;
                    }
                }
                else
                {
                    var possibleIndex = YPos * City.YMax + (XPos - numOfSpaces);
                    if (XPos - numOfSpaces >= 0
                        && XPos - numOfSpaces >= GetTargetXPos(prospectivePassenger))
                    {
                        XPos -= numOfSpaces;

                        UpdateCityPosition(oldXPos, YPos);
                        return true;
                    }
                }

                return MoveInXDirection(prospectivePassenger, numOfSpaces - 1, isRight);
            }

            return false;
        }

        public bool MoveInYDirection(Passenger prospectivePassenger, int numOfSpaces, bool isDown)
        {
            if (numOfSpaces > 0)
            {
                var oldYPos = YPos;

                if (isDown)
                {
                    var possibleIndex = (YPos + numOfSpaces) * City.YMax + XPos;
                    if (YPos + numOfSpaces <= City.YMax
                        && YPos + numOfSpaces <= GetTargetYPos(prospectivePassenger))
                    {
                        YPos += numOfSpaces;

                        UpdateCityPosition(XPos, oldYPos);
                        return true;
                    }
                }
                else
                {

                    var possibleIndex = (YPos - numOfSpaces) * City.YMax + XPos;
                    if (YPos - numOfSpaces >= 0
                        && YPos - numOfSpaces >= GetTargetYPos(prospectivePassenger))
                    {
                        YPos -= numOfSpaces;

                        UpdateCityPosition(XPos, oldYPos);
                        return true;
                    }
                }

                return MoveInYDirection(prospectivePassenger, numOfSpaces - 1, isDown);
            }

            return false;
        }

        private void UpdateCityPosition(int oldXPos, int oldYPos)
        {
            City.CityPositions[oldYPos * City.YMax + oldXPos].Car = null;
            City.CityPositions[YPos* City.YMax + XPos].Car = this;

            if (HasPassenger)
            {
                City.CityPositions[oldYPos * City.YMax + oldXPos].Passenger = null;
                City.CityPositions[YPos * City.YMax + XPos].Passenger = Passenger;
                Passenger.StartingXPos = XPos;
                Passenger.StartingYPos = YPos;
            }
        }
    }
}
