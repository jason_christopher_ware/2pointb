﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using TransportProj.Common;
using TransportProj.Common.Enums;
using TransportProj.Common.Interfaces;
using TransportProj.FactorySelection;

namespace TransportProj
{
    class Program
    {
        private const string _2PointBUrl = "http://2pointb.com/";
        private const int _webRequestTimeout = 15000;

        public static void Main(string[] args)
        {
            int CityWidth = 10;
            int CityHeight = 10;

            // demonstrating decoupling implementations of abstract classes to allow for easier testability, refactoring, and extensibility
            ICarFactory carFactory = CarFactorySelector.GetCarFactory();
            ICityFactory cityFactory = CityFactorySelector.GetCityFactory();

            var MyCity = cityFactory.Create(carFactory, CityWidth, CityHeight);
            var car = MyCity.Car;
            var passenger = MyCity.Passenger;

            while (!passenger.IsAtDestination())
            {
                TickAsync(car, passenger).Wait();

                // Verifying City Position is updating along with Car and Passenger
                for (int xCol = 0; xCol < CityWidth; xCol++)
                {
                    for (int yRow = 0; yRow < CityHeight; yRow++)
                    {
                        var position = MyCity.CityPositions[yRow * CityHeight + xCol];
                        if (position.Car != null)
                        {
                            Console.WriteLine(String.Format("****Car Found In City Position - X: {0}, Y: {1}", position.Car.XPos, position.Car.YPos));
                        }
                        if (position.Passenger != null)
                        {
                            Console.WriteLine(String.Format("****Passenger Found In City Position - X: {0}, Y: {1}", position.Passenger.GetCurrentXPos(), position.Passenger.GetCurrentYPos()));

                        }
                    }
                }
            }

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static async Task TickAsync(Car car, Passenger passenger)
        {
            var hasCarMoved = false;

            DirectionToMove directionToMove = GetDirectionToMove(car, passenger);

            switch (directionToMove)
            {
                case DirectionToMove.DoNothing:
                    break;
                case DirectionToMove.Left:
                    car.MoveLeft(passenger);
                    hasCarMoved = true;
                    break;
                case DirectionToMove.Right:
                    car.MoveRight(passenger);
                    hasCarMoved = true;
                    break;
                case DirectionToMove.Up:
                    car.MoveUp(passenger);
                    hasCarMoved = true;
                    break;
                case DirectionToMove.Down:
                    car.MoveDown(passenger);
                    hasCarMoved = true;
                    break;
                case DirectionToMove.Arrived:
                    if (!car.HasPassenger)
                    {
                        passenger.GetInCar(car);
                    }
                    else
                    {
                        passenger.GetOutOfCar();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (hasCarMoved)
            {
                Console.WriteLine("\tCar moved, downloading 2PointB Website");
                var downloadedBytes = await Task.Run(() => Get2PointBWebsiteInBytesAsync());
            }
        }

        private static DirectionToMove GetDirectionToMove(Car car, Passenger passenger)
        {
            int targetXPos = car.GetTargetXPos(passenger);
            int targetYPos = car.GetTargetYPos(passenger);

            int distanceXPos = Math.Abs(targetXPos - car.XPos);
            int distanceYPos = Math.Abs(targetYPos - car.YPos);

            if (distanceXPos == 0 && distanceYPos == 0)
            {
                return DirectionToMove.Arrived;
            }
            if (distanceYPos > distanceXPos)
            {
                return car.YPos > targetYPos
                    ? DirectionToMove.Down
                    : DirectionToMove.Up;
            }

            return car.XPos > targetXPos
                ? DirectionToMove.Left
                : DirectionToMove.Right;
        }

        private static async Task<byte[]> Get2PointBWebsiteInBytesAsync()
        {

            var content = new MemoryStream();
            var webRequest = (HttpWebRequest)WebRequest.Create(_2PointBUrl);
            webRequest.Timeout = _webRequestTimeout;

            try
            {
                using (WebResponse response = webRequest.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            responseStream.CopyTo(content);
                        }
                    }
                }
            }
            catch (WebException)
            {
                Console.WriteLine(String.Format("\t\tWebsite took longer than {0} ms, skipping downloading the website", _webRequestTimeout));

            }
            catch (Exception)
            {
                Console.WriteLine(String.Format("\t\tAn error occurred contacint the Website, skipping downloading the website", _webRequestTimeout));
            }

            return content.ToArray();
        }
    }
}
