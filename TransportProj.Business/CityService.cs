﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using TransportProj.Common;
using TransportProj.Common.Enums;
using TransportProj.Common.Interfaces;
using TransportProj.Dto;
using TransportProj.Dto.Dtos;
using TransportProj.Dto.Models;
using TransportProj.FactorySelection;

namespace TransportProj.Business
{
    // Having a service seperate where the logic is done allows for easier unit testing
    public class CityService
    {
        private const string _2PointBUrl = "http://2pointb.com/";
        private const int _webRequestTimeout = 15000;

        private readonly ICityFactory _cityFactory;
        private readonly ICarFactory _carFactory;


        public CityService()
        {
            _cityFactory = CityFactorySelector.GetCityFactory();
            _carFactory = CarFactorySelector.GetCarFactory();

            Mapper.CreateMap<City, CityDto>();
            Mapper.CreateMap<Passenger, PassengerDto>();
            Mapper.CreateMap<Car, CarDto>();
            Mapper.CreateMap<CityPosition, CityPositionDto>();
        }

        public ResultDto<CityViewModel> GenerateCity(int cityWidth, int cityHeight)
        {
            var result = new ResultDto<CityViewModel>();

            var city = _cityFactory.Create(_carFactory, cityWidth, cityHeight);
            if (city != null)
            {

                var cityDto = Mapper.Map<CityDto>(city);
                var passengerDto = Mapper.Map<PassengerDto>(city.Passenger);
                var carDto = Mapper.Map<CarDto>(city.Car);

                var cityPositionsLength = cityWidth * cityHeight;
                cityDto.CityPositionDtos = new CityPositionDto[cityPositionsLength];
                for (int i = 0; i < cityPositionsLength; i++)
                {
                    cityDto.CityPositionDtos[i] = Mapper.Map<CityPositionDto>(city.CityPositions[i]);
                }

                var carPos = carDto.YPos * cityDto.YMax + carDto.XPos;
                var passengerPos = passengerDto.StartingYPos * cityDto.YMax + passengerDto.StartingXPos;

                cityDto.CityPositionDtos[carPos].CarDto = carDto;
                cityDto.CityPositionDtos[passengerPos].PassengerDto = passengerDto;

                var viewModel = new CityViewModel
                {
                    CityDto = cityDto
                };

                result.Data = viewModel;
                result.IsSuccessful = true;
            }

            return result;
        }

        public CityViewModel CreateCityViewModel(City city, int cityWidth, int cityHeight)
        {
            var cityDto = Mapper.Map<CityDto>(city);
            var passengerDto = Mapper.Map<PassengerDto>(city.Passenger);
            var carDto = Mapper.Map<CarDto>(city.Car);

            var cityPositionsLength = cityWidth * cityHeight;
            cityDto.CityPositionDtos = new CityPositionDto[cityPositionsLength];
            for (int i = 0; i < cityPositionsLength; i++)
            {
                cityDto.CityPositionDtos[i] = Mapper.Map<CityPositionDto>(city.CityPositions[i]);
            }

            var carPos = carDto.YPos * cityDto.YMax + carDto.XPos;
            var passengerPos = passengerDto.StartingYPos * cityDto.YMax + passengerDto.StartingXPos;

            cityDto.CityPositionDtos[carPos].CarDto = carDto;
            cityDto.CityPositionDtos[passengerPos].PassengerDto = passengerDto;

            var viewModel = new CityViewModel
            {
                CityDto = cityDto
            };

            return viewModel;
        }

        public ResultDto<CityViewModel> Tick(CityViewModel cityViewModel)
        {
            var result = new ResultDto<CityViewModel>();
            if (cityViewModel != null)
            {
                var city = _cityFactory.Create(_carFactory, cityViewModel.CityDto.XMax, cityViewModel.CityDto.YMax);
                var destinationPos = city.CityPositions.First(c => c.IsDestinationPosition);
                destinationPos.IsDestinationPosition = false;

                var carDto = cityViewModel.CityDto.CityPositionDtos.First(c => c.CarDto != null).CarDto;
                var passengerDto = cityViewModel.CityDto.CityPositionDtos.First(c => c.PassengerDto != null).PassengerDto;

                var car = city.AddCarToCity(carDto.CarType, carDto.XPos, carDto.YPos);
                var passenger = city.AddPassengerToCity(passengerDto.StartingXPos, passengerDto.StartingYPos, passengerDto.DestinationXPos, passengerDto.DestinationYPos);

                if (car.XPos == passenger.StartingXPos
                    && car.YPos == passenger.StartingYPos)
                {
                    car.PickupPassenger(passenger);
                }

                var hasCarMoved = false;

                DirectionToMove directionToMove = GetDirectionToMove(car, passenger);

                switch (directionToMove)
                {
                    case DirectionToMove.DoNothing:
                        break;
                    case DirectionToMove.Left:
                        car.MoveLeft(passenger);
                        hasCarMoved = true;
                        break;
                    case DirectionToMove.Right:
                        car.MoveRight(passenger);
                        hasCarMoved = true;
                        break;
                    case DirectionToMove.Up:
                        car.MoveUp(passenger);
                        hasCarMoved = true;
                        break;
                    case DirectionToMove.Down:
                        car.MoveDown(passenger);
                        hasCarMoved = true;
                        break;
                    case DirectionToMove.Arrived:
                        if (!car.HasPassenger)
                        {
                            passenger.GetInCar(car);
                        }
                        else
                        {
                            passenger.GetOutOfCar();
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (hasCarMoved)
                {
                    Task.Run(() => Get2PointBWebsiteInBytesAsync());
                }

                var newViewModel = CreateCityViewModel(city, cityViewModel.CityDto.XMax, cityViewModel.CityDto.YMax);
                result.Data = newViewModel;
                result.IsSuccessful = true;
            }

            return result;
        }

        private DirectionToMove GetDirectionToMove(Car car, Passenger passenger)
        {
            int targetXPos = car.GetTargetXPos(passenger);
            int targetYPos = car.GetTargetYPos(passenger);

            int distanceXPos = Math.Abs(targetXPos - car.XPos);
            int distanceYPos = Math.Abs(targetYPos - car.YPos);

            if (distanceXPos == 0 && distanceYPos == 0)
            {
                return DirectionToMove.Arrived;
            }
            if (distanceYPos > distanceXPos)
            {
                return car.YPos > targetYPos
                    ? DirectionToMove.Down
                    : DirectionToMove.Up;
            }

            return car.XPos > targetXPos
                ? DirectionToMove.Left
                : DirectionToMove.Right;
        }

        private async Task<byte[]> Get2PointBWebsiteInBytesAsync()
        {

            var content = new MemoryStream();
            var webRequest = (HttpWebRequest)WebRequest.Create(_2PointBUrl);
            webRequest.Timeout = _webRequestTimeout;

            try
            {
                using (WebResponse response = webRequest.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            responseStream.CopyTo(content);
                        }
                    }
                }
            }
            catch (WebException)
            {
                Console.WriteLine(String.Format("\t\tWebsite took longer than {0} ms, skipping downloading the website", _webRequestTimeout));

            }
            catch (Exception)
            {
                Console.WriteLine(String.Format("\t\tAn error occurred contacint the Website, skipping downloading the website", _webRequestTimeout));
            }

            return content.ToArray();
        }
    }
}
