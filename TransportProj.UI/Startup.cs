﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TransportProj.UI.Startup))]
namespace TransportProj.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
