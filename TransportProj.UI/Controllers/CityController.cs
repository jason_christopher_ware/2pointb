﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web.Mvc;
using TransportProj.Dto;
using TransportProj.Dto.Models;

namespace TransportProj.UI.Controllers
{
    public class CityController : Controller
    {
        private const string HostUrl = "http://localhost:11702/";
        private const string ApiPrefix = "api/City";

        private const int CityWidth = 10;
        private const int CityHeight = 10;

        // GET: City
        public async Task<ActionResult> CityView()
        {
            var result = await CallApiGet<CityViewModel>("GenerateCity", CityWidth, CityHeight);

            if (result.IsSuccessful)
            {
                var k = result.Data.CityDto.CityPositionDtos.FirstOrDefault(c => c.CarDto != null);
                return View(result.Data);
            }

            return View(new CityViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Tick(CityViewModel city)
        {
            var result = await CallApiPost("Tick", city);

            if (result.IsSuccessful)
            {
                return View("CityView", result.Data);
            }

            return View("CityView", new CityViewModel());
        }

        public async Task<ResultDto<T>> CallApiGet<T>(string method, params object[] args)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(HostUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var url = String.Format("{0}/{1}/{2}", ApiPrefix, method, String.Join("/", args));

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<ResultDto<T>>();
                }

                return new ResultDto<T>();
            }
        }

        public async Task<ResultDto<T>> CallApiPost<T>(string method, T model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(HostUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var url = String.Format("{0}/{1}", ApiPrefix, method);

                var response = await client.PostAsJsonAsync(url, model);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<ResultDto<T>>();
                }

                return new ResultDto<T>();
            }
        }
    }
}