﻿using System;
using TransportProj.Common;
using TransportProj.Common.Enums;
using TransportProj.Common.Interfaces;

namespace TransportProj.Data.Factories
{
    public class CarFactory : ICarFactory
    {
        public Car Create(CarType type, int xPos, int yPos, City city, Passenger passenger)
        {
            Car car;

            switch (type)
            {
                case CarType.Sedan:
                    car = new Sedan(xPos, yPos, city, passenger);
                    break;
                case CarType.RaceCar:
                    car = new RaceCar(xPos, yPos, city, passenger);
                    break;
                case CarType.Motorcycle:
                    car = new Motorcycle(xPos, yPos, city, passenger);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            return car;
        }
    }
}
