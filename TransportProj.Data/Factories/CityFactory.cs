﻿using System;
using System.Linq;
using TransportProj.Common;
using TransportProj.Common.Enums;
using TransportProj.Common.Interfaces;

namespace TransportProj.Data.Factories
{
    public class CityFactory : ICityFactory
    {
        private readonly Random _rand = new Random();

        public City Create(ICarFactory carFactory, int cityWidth, int cityHeight)
        {
            int[] BuildingPositions = new[]
            {
                0 * cityWidth + 1,
                0 * cityWidth + 5,
                1 * cityWidth + 3,
                4 * cityWidth + 0,
                4 * cityWidth + 1,
                4 * cityWidth + 8,
                4 * cityWidth + 9,
                6 * cityWidth + 0,
                6 * cityWidth + 1,
                6 * cityWidth + 6,
                6 * cityWidth + 7,
                6 * cityWidth + 8,
                7 * cityWidth + 7,
                7 * cityWidth + 8,
                9 * cityWidth + 0,
                9 * cityWidth + 2,
                9 * cityWidth + 3,
                9 * cityWidth + 4,
            };

            int xCarPos, yCarPos, xPassengerPos, yPassengerPos, xDestinationPos, yDestinationPos;

            GenerateRandomPosition(BuildingPositions, cityWidth, cityHeight, out xCarPos, out yCarPos);
            GenerateRandomPosition(BuildingPositions, cityWidth, cityHeight, out xPassengerPos, out yPassengerPos);
            GenerateRandomPosition(BuildingPositions, cityWidth, cityHeight, out xDestinationPos, out yDestinationPos);

            City city = new City(carFactory, cityWidth, cityHeight, BuildingPositions);
            city.AddCarToCity(CarType.Sedan, xCarPos, yCarPos);
            city.AddPassengerToCity(xPassengerPos, yPassengerPos, xDestinationPos, yDestinationPos);

            return city;
        }

        private void GenerateRandomPosition(int[] buildingPositions, int xMax, int yMax, out int xPos, out int yPos)
        {
            do
            {
                xPos = _rand.Next(xMax - 1);
                yPos = _rand.Next(yMax - 1);
            } while (buildingPositions.Contains(xPos * xMax + yPos));
        }
    }
}
