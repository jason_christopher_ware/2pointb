﻿using System;
using TransportProj.Common;

namespace TransportProj.Data
{
    class Motorcycle : Car
    {
        private const int MaxXSpacesCanMove = 1;
        private const int MaxYSpacesCanMove = 3;

        public Motorcycle(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp(Passenger prospectivePassenger)
        {
            if (MoveInYDirection(prospectivePassenger, MaxYSpacesCanMove, true))
            {
                WritePositionToConsole();
            }
        }

        public override void MoveDown(Passenger prospectivePassenger)
        {
            if (MoveInYDirection(prospectivePassenger, MaxYSpacesCanMove, false))
            {
                WritePositionToConsole();
            }
        }

        public override void MoveRight(Passenger prospectivePassenger)
        {
            if (MoveInXDirection(prospectivePassenger, MaxXSpacesCanMove, true))
            {
                WritePositionToConsole();
            }
        }

        public override void MoveLeft(Passenger prospectivePassenger)
        {
            if (MoveInXDirection(prospectivePassenger, MaxXSpacesCanMove, false))
            {
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Motorcycle moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
