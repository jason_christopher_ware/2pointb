﻿using System;
using TransportProj.Common;

namespace TransportProj.Data
{
    public class Sedan : Car
    {
        private const int MaxSpacesCanMove = 1;

        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp(Passenger prospectivePassenger)
        {
            if (MoveInYDirection(prospectivePassenger, MaxSpacesCanMove, true))
            {
                WritePositionToConsole();
            }
        }

        public override void MoveDown(Passenger prospectivePassenger)
        {
            if (MoveInYDirection(prospectivePassenger, MaxSpacesCanMove, false))
            {
                WritePositionToConsole();
            }
        }

        public override void MoveRight(Passenger prospectivePassenger)
        {
            if (MoveInXDirection(prospectivePassenger, MaxSpacesCanMove, true))
            {
                WritePositionToConsole();
            }
        }

        public override void MoveLeft(Passenger prospectivePassenger)
        {
            if (MoveInXDirection(prospectivePassenger, MaxSpacesCanMove, false))
            {
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
