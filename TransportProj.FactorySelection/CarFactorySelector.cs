﻿using TransportProj.Common.Interfaces;
using TransportProj.Data.Factories;

namespace TransportProj.FactorySelection
{
    public static class CarFactorySelector
    {
        // very simple method just to demonstrate how we can decouple the references to our *.Data project
        // this allows for easier refactoring and keeps our main project from not needing to rely on a specifc data implementation (i.e. not reliant on an ORM)
        // Usually this would be done in a Repository type of pattern
        public static ICarFactory GetCarFactory()
        {
            return new CarFactory();
        }
    }
}
