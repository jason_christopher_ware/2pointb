﻿using TransportProj.Common.Interfaces;
using TransportProj.Data.Factories;

namespace TransportProj.FactorySelection
{
    public static class CityFactorySelector
    {
        public static ICityFactory GetCityFactory()
        {
            return new CityFactory();
        }
    }
}
